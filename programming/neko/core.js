////////////////////////////////////////////////////////////////////////////////
// NOTE on recursion depth:
//      for reasons i don't yet understand, there is a difference between
//      `defn broken { broken }` // InternalError: too much recursion (1700)
//      and
//      `defn ok { 0 pop ok }`   // happily consumes CPU for eternity
//      from the standpoint of JS recursion.

////////////////////////////////////////////////////////////////////////////////
// TODO: does the REPL still need to track the highlights?

////////////////////////////////////////////////////////////////////////////////
// for debugging purposes

////////////////////////////////////////
// positions and origins

function Position(row, column, cursor) {
    this.row = row;
    this.column = column;
    this.cursor = cursor;
};

Position.prototype.string = function () {
    return `r${this.row}:c${this.column}`;
};

// TODO: this should probably be used anywhere a position is passed
Position.prototype.clone = function () {
    return new Position(this.row, this.column, this.cursor);
};

function Origin(from, info) {
    this.from = from;
    this.info = {};
    for (let key in info) { this.info[key] = info[key] };
};

function parse_origin(parser) {
    return new Origin("Neko parser", {
        start: parser.tokens[parser.cursor].start,
        end: parser.tokens[parser.cursor].end,
    });
};

////////////////////////////////////////
// errors

function Blunder(message, hint) {
    this.error = message;
    this.hint = hint;
};
Blunder.prototype.add_info = function (info) {
    for (let key in info) { this[key] = info[key]; };
};

function NekoError(message, hint, emulator, ...args) {
    Blunder.apply(this, arguments);
    this.type = "neko";
    this.args = args;
    this.callstack = emulator.callstack;
    this.defns = emulator.definitions;
    emulator.callstack = [];
};
NekoError.prototype = Blunder.prototype;

NekoError.prototype.string = function () {
    return `Evaluation Error: ${this.error}\nHint: ${this.hint.replace(/\n/g, "\n      ")}\nArguments: ${this.args.map(JSON.stringify).join(' ')}\nCallstack:\n   ${this.callstack.map(x => x.string(this.defns)).join("\n    > ")}`;
};

NekoError.prototype.html = function () {
    return `<div class='stderr'><b>Evaluation Error</b>: ${this.error}</div><div class="stderr">\n<b>Hint</b>: ${this.hint.replace(/\n/g, "<br>&nbsp; &nbsp; &nbsp; ")}</div><div class='stderr'><b>Arguments</b>: ${this.args.map(JSON.stringify).join(' ')}</div><div class='stderr'><b>Callstack</b>:<br>&nbsp;&nbsp;&nbsp;${this.callstack.map(x => x.html(this.defns)).join("<br><span class='nekodelimiter'>&nbsp;&nbsp;&nbsp;&nbsp;&gt;</span> ")}</div>`
};

function ParseError(message, hint, start, end) {
    Blunder.apply(this, arguments);
    this.type = "parse";
    this.start = start;
    this.end = end;
};
// ParseError.prototype = Blunder.prototype;

ParseError.prototype.string = function () {
    return `Parse Error: ${this.error} @ ${this.start.string()} -- ${this.end.string()}\nHint: ${this.hint.replace(/\n/g, "\n      ")}`;
};

ParseError.prototype.html = function () {
    return `<div class='stderr'><b>Parse Error</b>: ${this.error} @ ${this.start.string()} -- ${this.end.string()}<br><b>Hint</b>: ${this.hint.replace(/\n/g, "<br>&nbsp; &nbsp; &nbsp; ")}</div>`;
};

////////////////////////////////////////////////////////////////////////////////
// calls

function Call(neko, site) {
    this.neko = neko;
    this.site = site + 1;
};

// TODO: add a verbose_callstack option
Call.prototype.string = function (defns) {
    let num = isNaN(this.site) ? "" : ` @ ${this.site}`;
    let post;
    if (this.neko.type == "identifier") {
        post = defns[this.neko.value] === undefined ? "" : ` { ${nekos_to_string(defns[this.neko.value])} }`;
    } else {
        post = "";
    };
    return `${this.neko.string()}${post}${num}`;
};

Call.prototype.html = function (defns) {
    let num = isNaN(this.site) ? "" : ` @ ${this.site}`;
    let post;
    if (this.neko.type == "identifier") {
        post = defns[this.neko.value] === undefined ? "" : ` ${stylize_html("delimiter", "{")} ${defns[this.neko.value].commands.map(x => x.html()).join(' ')} ${stylize_html("delimiter", "}")}`;
    } else {
        post = "";
    };
    return `${this.neko.html()}${post}${num}`;
};


////////////////////////////////////////////////////////////////////////////////
// types

function Neko(type, value, origin) {
    this.type = type;
    this.value = value;
    this.origin = origin;
};

Neko.prototype.string = function () {
    switch (this.type) {
    case "boolean":
    case "integer":
    case "identifier":
    case "builtin":
        return "" + this.value;
    case "string":
        // TODO: this needs to correctly handle string delimiters and escapes...
        // e.g., '"' won't be printed correctly
        return "\"" + this.value + "\"";
    case "quote":
        return "[" + nekos_to_string(this.value) + "]";
    default:
        return new NekoError(
            "unrecognized type",
            "this should be unreachable (contact a maintainer)",
            {}, this.type
        );
    };
};

// TODO: this needs to change if the string escaping issue is fixed
Neko.prototype.html = function () {
    switch (this.type) {
    case "boolean":
    case "integer":
    case "identifier":
    case "builtin":
        return stylize_html(this.type, "" + this.value);
    case "string":
        // TODO: this needs to correctly handle string delimiters and character escapes...
        // e.g., '"' won't be printed correctly
        return stylize_html("string", "\"" + this.value + "\"");
    case "quote":
        return stylize_html("delimiter", "[") + this.value.map(x => x.html()).join(" ") + stylize_html("delimiter", "]");
    default:
        return new NekoError(
            "unrecognized type",
            "this should be unreachable (contact a maintainer)",
            {}, this.type
        );
    };
};

////////////////////////////////////////
// safe construction

function neko_number(value, builder, ...args) {
    let origin;
    switch (builder.type) {
    case "parser":
        origin = parse_origin(builder);
        if (isNaN(value) || value === Infinity) {
            // TODO: append a message about precision integer implementation
            return new ParseError(
                "number parse failed",
                "ask a maintainer to fast-track the new model for integers",
                origin.info.start,
                origin.info.end
            );
        };
        return new Neko("integer", value, origin);
    case "emulator":
        origin = Origin("Neko emulator", {
            caller: builder.last_call(),
            args: args,
        });
        if (isNaN(value) || value === Infinity) {
            // TODO: append a message about precision integer implementation
            return new NekoError(
                "number construction failed",
                "ask a maintainer to fast-track the new model for integers",
                builder, ...args,
            );
        };
        return new Neko("integer", value, origin);
    };
};

////////////////////////////////////////
// checks

// TODO: should this be a method?
function neko_equality_check(x, y) {
    if (x.type != y.type) {
        return false;
    } else {
        switch (x.type) {
        case "integer":
        case "boolean":
        case "string":
        case "builtin":
        case "identifier":
            return x.value == y.value;
            break;
        case "quote":
            if (x.value.length != y.value.length) { return false; };
            for (let i=0; i<x.value.length; i++) {
                let check = neko_equality_check(x.value[i], y.value[i]);
                if (check.error !== undefined) { return check; };
                if (! check) { return false; };
            };
            return true;
            break;
        default:
            return new NekoError(
                "equality not implemented for this type",
                "this should be unreachable (contact a maintainer)",
                {}, x.string, x.type, y.string,
            );
            break;
        };
    };
};


////////////////////////////////////////////////////////////////////////////////
// regex for lexing
// IMPORTANT:
// 1. don't forget to update types whenever you update the token_regex!
// 2. the regex must always match...
//    the `unrecognized` type is there so comments can be arbitrary

let token_regex = /^((defn)\b|(true|false)\b|(-?[0-9]+)\b|([a-zA-Z_][a-zA-Z_0-9]*)\b|(')|(")|(\[|\()|(\]|\))|(\{)|(\})|(\/\/)|(\n[ \t]*)|([ \t]+)|([^ \t\n"'\[\]\{\}\(\)]+))/;
let token_types = [
    null, null,
    "definition",
    "boolean",
    "integer",
    "identifier",
    "string-1", "string-2",
    "quote-open", "quote-close",
    "defn-open", "defn-close",
    "comment-line",
    "newline", "whitespace",
    "unrecognized",
];

let matching_paren = { '(':')', ')':'(', '[':']', ']':'[', '{':'}', '}':'{' };


////////////////////////////////////////////////////////////////////////////////
// lexer

function Token(type, text, start, end) {
    this.type = type;
    this.text = text;
    this.start = start;
    this.end = end;
};

function Lexer(text) {
    this.text = text;
    this.len = text.length;
    this.position = new Position(1, 1, 0);
};

Lexer.prototype.next_token = function () {
    const start = this.position.clone();
    const cursor = start.cursor;
    let rest = this.text.slice(cursor);
    if (rest.length == 0) { return null; };
    let ms = rest.match(token_regex);
    var type;
    this.position.cursor += ms[0].length;
    if (builtins[ms[0]] !== undefined) {
        type = "builtin";
        this.position.column += ms[0].length;
    } else {
        for (let i=2; i<token_types.length; i++) {
            if (ms[i] !== undefined) {
                type = token_types[i];
                if (type == "newline") {
                    this.position.row++;
                    this.position.column = ms[0].length;
                } else {
                    this.position.column += ms[0].length;
                };
                break;
            };
        };
    };
    return new Token(type, ms[0], start, this.position);
};

Lexer.prototype.tokenize = function () {
    var answer = [];
    var token = this.next_token();
    for (let i=1; (token !== null) && (token !== undefined); i++) {
        answer.push(token);
        token = this.next_token();
    };
    return answer;
};


////////////////////////////////////////////////////////////////////////////////
// parser

function Parser(tokens) {
    this.tokens = tokens;
    this.depth = [];
    this.answer = [[]];
    this.cursor = 0;
    this.definitions = {};
    this.html = "";
};
Parser.prototype.type = "parser";

Parser.prototype.expect = function (...types) {
    // NOTE: you have to feed this function the token that caused its call.
    //       otherwise it could cause a JS out-of-bounds error
    if (types.length > this.tokens.length - this.cursor) {
        return new ParseError(
            "not enough tokens",
            "the parser expected more tokens (check your syntax)",
            this.tokens[this.cursor].start,
            this.tokens[this.tokens.length-1].end,
        );
    };
    for (let i=0; i<types.length; i++) {
        if (this.tokens[this.cursor].type != types[i]) {
            let err = new ParseError(
                "expected token failure",
                `the parser expected a token of type \`${types[i]}\` but found \`${this.tokens[this.cursor].type}\``,
                this.tokens[this.cursor].start,
                this.tokens[this.cursor].end,
            );
            err.add_info({
                expected: types[i],
                actual: this.tokens[this.cursor].type,
            });
            return err;
        };
        this.cursor++;
    };
};
Parser.prototype.drop_comment = function () {
    let init = this.cursor;
    while ((this.cursor < this.tokens.length) && (this.tokens[this.cursor].type != "newline")) {
        this.cursor++;
    };
    return this.tokens
        .slice(init, this.cursor+1)
        .map((x) => { return x.text })
        .join('');
};
Parser.prototype.append_html = function (html) {
    this.html = this.html + html;
};
Parser.prototype.append_styled = function (style, html) {
    this.html = this.html + stylize_html(style, html);
};

// TODO: need to parser_expect space after certain tokens
//       - e.g. the end of a string
//       also need to be able to see them at EOF...

Parser.prototype.parse = function () {
    let tokens = this.tokens;
    while (this.cursor < tokens.length) {
        let type = tokens[this.cursor].type;
        switch (type) {
        case "definition":
            let start_position = tokens[this.cursor].start.clone();
            let start_parser = this.cursor;
            if (this.defining || this.depth.length != 0) {
                return new ParseError(
                    "nested definition",
                    "`defn` is not allowed within quotes or other `defn` blocks",
                    this.defining,
                    start_position,
                );
            };
            let check = this.expect(
                "definition",
                "whitespace",
                "identifier",
                "whitespace",
                "defn-open",
            );
            if (check !== undefined) { return check; };
            let name = this.tokens[this.cursor-3].text;
            // TODO: we need to carefully evaluate how this `site` is handled.
            //       at the moment it appears to be doing little.
            //       moreover, the current model does not accurately hold the
            //       information it is designed to hold (definition location)...
            let site = (this.definitions[name] === undefined) ? [] : this.definitions[name].site;
            site.push(start_position);
            while ((this.cursor < tokens.length) && (tokens[this.cursor].type != "defn-close")) {
                if (tokens[this.cursor].type == "comment-line") {
                    // comment will be recorded when the definition is parsed
                    this.drop_comment();
                };
                this.cursor++;
            };
            if (this.cursor >= tokens.length) {
                return new ParseError(
                    "unterminated defn",
                    "the parser found the end-of-file before the close of this `defn` block",
                    start_position,
                    this.tokens[this.tokens.length-1].end,
                );
            };
            var def_parser = new Parser(tokens.slice(start_parser + 5, this.cursor));
            def_parser.defining = start_position;
            var def = def_parser.parse();
            if (def.error !== undefined) { return def; };
            this.definitions[name] = {
                commands: def.commands,
                html: def.html,
                start: start_position,
                end: this.tokens[this.cursor].end,
                site: site,
            };
            this.append_html(`${stylize_html("keyword", "defn")} ${stylize_html("defname", name)} ${stylize_html("delimiter", "{")}${def.html}${stylize_html("delimiter", "}")}`);
            break;
        case "integer":
            var tmp = this.answer.pop();
            var num = tokens[this.cursor].text;
            var val = neko_number(parseInt(num), this);
            if (val.error !== undefined) { return val; };
            tmp.push(val);
            this.answer.push(tmp);
            this.append_styled(type, num);
            break;
        case "boolean":
            var tmp = this.answer.pop();
            var bool = tokens[this.cursor].text;
            tmp.push(new Neko(type, bool == "true", parse_origin(this)));
            this.answer.push(tmp);
            this.append_styled(type, bool);
            break;
        case "builtin":
        case "identifier":
            var tmp = this.answer.pop();
            var ident = tokens[this.cursor].text;
            tmp.push(new Neko(type, ident, parse_origin(this)));
            this.answer.push(tmp);
            this.append_styled(type, ident);
            break;
        case "string-1":
        case "string-2":
            // TODO: pass this information on to the constructed neko
            var mark = [`'`,`"`][parseInt(type[7])-1];
            var string = "";
            var init = tokens[this.cursor].start;
            var end = tokens[this.cursor].type;
            this.cursor++;
            while ((this.cursor < tokens.length) && (tokens[this.cursor].type != end)) {
                // TODO: handle escapes
                string = string + tokens[this.cursor].text;
                this.cursor++;
            };
            if (this.cursor >= tokens.length) {
                return new ParseError(
                    "unterminated string",
                    `a string delimited by ${mark} found no matching delimiter`,
                    init,
                    tokens[tokens.length-1].end,
                );
            };
            var tmp = this.answer.pop();
            tmp.push(new Neko("string", string, parse_origin(this)));
            this.answer.push(tmp);
            this.append_styled("string", mark + string + mark);
            break;
        case "quote-open":
            let delim = tokens[this.cursor];
            this.depth.push(delim);
            this.answer.push([]);
            this.append_styled("delimiter", delim.text);
            break;
        case "quote-close":
            let open = this.depth.pop();
            let close = tokens[this.cursor];
            if (open === undefined) {
                return new ParseError(
                    "unbalanced delimiter",
                    `an extra \`${close.text}\` was encountered`,
                    tokens[this.cursor].start,
                    tokens[this.cursor].end,
                );
            } else if (open.text !== matching_paren[close.text]) {
                return new ParseError(
                    "mismatched delimiters",
                    `quote beginning with \`${open.text}\` was terminated by \`${close.text}\``,
                    open.start,
                    close.end,
                );
            };
            var done = this.answer.pop();
            var tmp = this.answer.pop();
            tmp.push(new Neko("quote", done, parse_origin(this)));
            this.answer.push(tmp);
            this.append_styled("delimiter", close.text);
            break;
        case "defn-open":
        case "defn-close":
            return new ParseError(
                "misplaced defn delimiter",
                "encountered a `defn` delimiter outside of `defn` context",
                tokens[this.cursor].start,
                tokens[this.cursor].end,
            );
        case "comment-line":
            let comment = this.drop_comment();
            this.append_styled("comment", comment);
            break;
        case "newline":
        case "whitespace":
            let space = tokens[this.cursor].text;
            this.append_html(make_html(space));
            break;
        default:
            return new ParseError(
                "unrecognized token type",
                "this should be unreachable (contact a maintainer)",
                tokens[this.cursor].start,
                tokens[this.cursor].end,
            );
        };
        this.cursor++;
    };
    if (this.depth.length != 0) {
        var open = this.depth.pop();
        return new ParseError(
            "unmatched delimiter",
            `delimiter \`${open.text}\` was not terminated before end-of-file`,
            open.start,
            tokens[tokens.length-1].end,
        );
    };
    return {
        commands: this.answer[0],
        definitions: this.definitions,
        html: this.html,
    };
};

function parse_program(program) {
    let tokens = (new Lexer(program)).tokenize();
    if (tokens.error !== undefined) { return tokens; };
    return (new Parser(tokens)).parse();
};


////////////////////////////////////////////////////////////////////////////////
// emulator options
function OptionContainer(options) {
    for (let i=0; i<options.length; i++) {
        this[options[i][0]] = new Option(
            options[i][1], options[i][2], options[i][3]
        );
    };
};
function Option(value, description, hook) {
    this.value = value;
    this.description = description;
    this.hook = hook;
};
Option.prototype.run_hook = function () {
    if (this.hook !== undefined) { this.hook(); };
};

// TODO: fix verbosity options implemention
//   ^^^ big job...
function emulator_default_options() {
    return new OptionContainer(
        [["show_stack",     true,
          "Toggles showing the stack below the REPL",
          function () {repl.sync_stack()}],
         ["vertical_stack", true,
          "Toggles showing the stack as arranged vertically",
          function () {repl.sync_stack()}],
         ["log_master",     false,
          "Toggles all browser console logging"],
         // ["log_while",      false,
         //  "Toggles console logging during `while`"],
         // ["log_apply",      false,
         //  "Toggles console logging in `apply`"],
         ["log_call",       false,
          "Toggles console logging of commands"],
         // ["log_expansion",  false,
         //  "Toggles console logging when `defn`s run"],
         ["log_definition", false,
          "Toggles console logging of `defn` keywords"],
         ["log_errors",     false,
          "Toggles console logging of errors"],
         ["max_while_iter", Infinity,
          "Maximum iterations in `while` loops"],
         ["max_call_depth", Infinity,
          "Maximum depth of the Neko callstack"],
         ["allow_redefine", false,
          "Toggles `defn` overwrites"],
         // ["allow_impure",   false,
         //  "Toggles whether impure functions can run"],
        ]);
};

////////////////////////////////////////////////////////////////////////////////
// Emulators

function Emulator(options = emulator_default_options()) {
    this.imports = {};
    this.definitions = {};
    this.defn_update = false;
    this.stack = [];
    this.options = options;
    this.callstack = [];
};

Emulator.prototype.type = "emulator";

////////////////////////////////////////
// options

Emulator.prototype.get_opt = function (opt) {
    return this.options[opt] ? this.options[opt].value : undefined;
};

Emulator.prototype.set_opt = function (opt, val) {
    this.options[opt].value = val;
};

////////////////////////////////////////
// stack manipulation

// TODO: accept an optional argument to pop more than one element
Emulator.prototype.pop = function () { return this.stack.pop(); };

Emulator.prototype.peek = function (pos) {
    return this.stack[this.stack.length - pos];
};

Emulator.prototype.push = function (neko) { this.stack.push(neko); };

////////////////////////////////////////
// callstack

// TODO: `while` should include iteration number

Emulator.prototype.last_call = function () {
    return this.callstack[this.callstack.length-1].neko;
};

Emulator.prototype.depth = function () { return this.callstack.length; };

Emulator.prototype.push_call = function (neko, num) {
    this.callstack.push(new Call(neko, num));
};

Emulator.prototype.pop_call = function () { this.callstack.pop(); };

////////////////////////////////////////
// checks

Emulator.prototype.check_length = function (required, ...args) {
    if (this.stack.length < required) {
        let err = new NekoError(
            "length check failed",
            `expected height at least ${required}, actual height is ${this.stack.length}\nsee the documentation for \`${this.last_call().string()}\``,
            this, ...args.map(x => x.string()),
        );
        err.add_info({ expected: required, actual: this.stack.length });
        return err;
    };
};

Emulator.prototype.check_types = function (...types) {
    // TODO: add `arguments` by slicing the stack
    for (let i=0; i<types.length; i++) {
        let type = types[i];
        // stop checking at `first` undefined (signals later types don't matter)
        if (type === undefined) { break; }
        // `null` signals that this type does not matter
        else if (type === null) { continue; }
        // error if the type expected doesn't match the given type
        else if (this.peek(i+1).type != type) {
            let err = new NekoError(
                `type check failed @ ${i+1}`,
                `expected ${type}, found ${this.peek(i+1).type}\nsee the documentation for \`${this.last_call().string()}\``,
                this, ...this.stack.slice(this.stack.length-types.length).map(x => x.string()).reverse(),
            );
            err.add_info({
                expected: types,
                actual: this.stack.slice(this.stack.length - types.length).toReversed(),
            });
            return err;
        };
    };
};

Emulator.prototype.check_stack = function (...types) {
    // check the length is correct
    let check = this.check_length(types.length);
    if (check !== undefined) { return check; }
    // then check the types
    return this.check_types(...types)
};

////////////////////////////////////////
// runners

// merge `defn`s into the dictionary
Emulator.prototype.merge_defns = function (definitions) {
    for (let name in definitions) {
        if (this.get_opt("allow_redefine") ||
            ((this.definitions[name] === undefined) &&
             (definitions[name].site.length === 1))) {
            this.definitions[name] = definitions[name];
        } else {
            return new NekoError(
                "redefinition attempted",
                "change `allow_redefine` option via `setopt`",
                this, name,
            );
        };
        if (this.get_opt("log_definition")) {
            console.log(`defn ${name} { ${nekos_to_string(definitions[name].commands, true)} }`);
        };
        this.defn_update = true;
    };
};

// run a single neko
Emulator.prototype.run_neko = async function (neko) {
    // NOTE: this function must be awaited every time it is called
    //       otherwise, `load` calls won't execute at the appropriate time
    if (this.depth() > this.get_opt("max_call_depth")) {
        return new NekoError(
            `recursion depth exceeded (${this.get_opt("max_call_depth")})`,
            "change `max_call_depth` option via `setopt`",
            this,
        );
    };
    if (this.get_opt("log_call")) {
        console.log("  ".repeat(this.depth()) + neko.string());
    };
    let result;
    switch (neko.type) {
    case "boolean":
    case "integer":
    case "string":
    case "quote":
        this.push(neko);
        break;
    case "builtin":
        result = await builtins[neko.value].action(this);
        if (result !== undefined) { return result; };
        break;
    case "identifier":
        let defn = this.definitions[neko.value];
        if (defn === undefined) {
            return new NekoError(
                "unrecognized name",
                "you either misspelled this name or did not provide its `defn`",
                this, neko.value,
            );
        };
        result = await this.apply(defn.commands);
        if (result !== undefined) { return result; };
        break;
    default:
        return new NekoError(
            "unrecognized command type",
            "this should be unreachable (contact a maintainer)",
            this, neko.string(), neko.type,
        );
    };
    if (neko.type != "identifier" && this.get_opt("log_master")) {
        console.log(`( ${nekos_to_string(this.stack, true)} )`);
    };
};

// run a list of nekos, taking care of the callstack
Emulator.prototype.apply = async function (operations) {
    // this runner tracks the position in the operation list
    // this is important for error reporting
    for (let i=0; i<operations.length; i++) {
        this.push_call(operations[i], i);
        let result = await this.run_neko(operations[i]);
        if (result !== undefined) { return result; };
        this.pop_call();
    };
};

// merge `defn`s and then run nekos
Emulator.prototype.run = async function (operations, definitions = {}) {
    let result = this.merge_defns(definitions);
    if (result !== undefined) { return result; };
    result = await this.apply(operations);
    if (result !== undefined) { return result; };
    return this;
};


////////////////////////////////////////////////////////////////////////////////
// TODO: finish implementing this
function Repl() {
    Emulator.apply(this, arguments);
    this.set_opt("allow_redefine", true);
    this.set_opt("max_while_iter", 10000);
    this.set_opt("max_call_depth", 1500);
    this.history = [];
    this.definitions = {};
    this.html = {};
    let elts = ["stdout", "stdin", "defns", "stack", "activeprompt"];
    for (let i=0; i<elts.length; i++) {
        this.html[elts[i]] = document.getElementById(elts[i]);
    };
    this.html.activeprompt.innerHTML = prompt;
};
Repl.prototype = Emulator.prototype;

////////////////////////////////////////
// options

Repl.prototype.update_opt = function (type, opt, val) {
    let v;
    let disp;
    switch (type) {
    case "boolean":
        document.getElementById(opt).checked = val;
        v = val;
        disp = "" + val;
        break;
    case "number":
        let n = parseInt(val);
        v = n >= 0 ? n : repl.get_opt(opt);
        document.getElementById(opt).value = "" + val;
        disp = "" + v;
        break;
    default:
        console.log("Option type not implemented...  Contact maintainer.");
        repl.stderr("Option type not implemented...  Contact maintainer.");
        break;
    };
    this.set_opt(opt, v);
    document.getElementById(`${opt}_display`).innerHTML = "" + disp;
    this.run_opt_hook(opt);
};

Repl.prototype.run_opt_hook = function (opt) {
    this.options[opt].run_hook();
};

////////////////////////////////////////
// printing

// TODO: update these

Repl.prototype.stdout = function (html) {
    this.html.stdout.innerHTML = this.html.stdout.innerHTML + `<div class="stdout">${html}</div>`;
};

Repl.prototype.stderr = function (html) {
    // TODO: this should accept an error object and handle things from there
    this.html.stdout.innerHTML = this.html.stdout.innerHTML + `<div class="stderr">Error: ${html}</div>`;
};

// TODO: are there useful stdin functions to write here?

////////////////////////////////////////
// interface

Repl.prototype.sync_defns = function () {
    if (this.defn_update) {
        this.html.defns.innerHTML = definitions_pretty_html(this.definitions);
        this.defn_update = false;
    };
};

Repl.prototype.sync_stack = function () {
    if (this.get_opt("show_stack")) {
        this.html.stack.style.display = "block";
        let sp, op, cl;
        if (this.get_opt("vertical_stack")) {
            sp = "<br>";
            op = "";
            cl = "";
        } else {
            sp = " ";
            op = "(";
            cl = ")";
        };
        this.html.stack.innerHTML = `${op}${sp}${this.stack.map(x => x.html()).reverse().join(sp)}${sp}${cl}`;
    } else {
        this.html.stack.style.display = "none";
    };
};

////////////////////////////////////////
// repl setup

// TODO: should some of these these be methods of Repl object?

let prompt = "&gt;&gt;&gt; ";
let initial_program = `
defn iload {
  "Type the path of the code to load." stdout stdin
  dup "..." swap concat "Loading " concat stdout
  load "Load finished!" stdout
}
`

// setup the repl
function repl_initialize() {
    repl = new Repl();
    repl.html.stdin.addEventListener("keydown", repl_listener);
    // do a page wipe for old definitions and history
    repl.html.defns.innerHTML = "";
    repl.html.stdout.innerHTML = "";
    // define some useful functions for the REPL
    repl_inject(initial_program, false);
    // test_error_reporting();
    repl.html.stdin.value = `\"lib/stdlib\" load     // loads the standard library`;
    repl_options_initialize_html();
};

async function repl_listener(e) {
    switch (e.key) {
    case "Enter":
        await repl_inject(repl.html.stdin.value.trim(), true);
        repl.html.stdin.value = "";
        break;
    default:
    };
};

// inject code into the repl for evaluation
async function repl_inject(code, show) {
    if (code.length != 0) {     // if the injection was not empty
        // prevent stdin from sending signals
        repl.html.stdin.disabled = true;
        // append the code snippet to the history
        repl.history.push(code);
        // parse the code
        let parse = parse_program(code);
        // log parser errors if they happen
        if (parse.error !== undefined && show === true) {
            // log unhighlighted code and the error
            repl.stdout(`<span class="stderr" onclick="repl.html.stdin.value=repl.history[${repl.history.length-1}]; repl.html.stdin.focus();"><span class="prompt">${prompt}</span>${make_html(code)}</span>`);
            repl.stdout(parse.html());
            if (repl.get_opt("log_errors")) {
                console.log(parse.string());
            };
        } else {
            // print syntax highlighted code (unless execution was silent)
            if (show === true) {
                repl.stdout(`<span onclick="repl.html.stdin.value=repl.history[${repl.history.length-1}]; repl.html.stdin.focus();"><span class="prompt">${prompt}</span>${parse.html}</span>`);
            };
            // run the parsed commands
            let result = await repl.run(parse.commands, parse.definitions);
            // if definitions were updated, redraw the element
            repl.sync_defns();
            // append errors to stdout with callstack (unless execution silent)
            if (result.error !== undefined && show === true) {
                // TODO: make the callstack an expandable element to save on space
                repl.stdout(result.html());
                if (repl.get_opt("log_errors")) {
                    console.log(result.string());
                };
            };
        };
        // update the stack
        repl.sync_stack();
        // move stdin back into view
        repl.html.stdin.scrollIntoView(false);
        window.scrollBy(0, 50);
        // re-enable stdin input
        repl.html.stdin.disabled = false;
        repl.html.stdin.focus();
    };
};

// allows us to run code as if we were typing at the prompt
// the last element of codes is left as is in the prompt
// this is very useful for style testing
async function repl_inject_stdin(...codes) {
    if (codes.length == 0) {
        return;
    };
    for (let i=0; i<codes.length-1; i++) {
        await repl_inject(codes[i].trim(), true);
    };
    repl.html.stdin.value = codes[codes.length-1];
    // restore lost focus after input toggled
    repl.html.stdin.focus();
};

////////////////////////////////////////
// generate initial options HTML for the page
function repl_options_initialize_html() {
    // TODO: make this a Repl method?
    let html = "<table><tr><th>Name</th><th>Current</th><th>Set</th><th>Description</th></tr>";
    for (opt in repl.options) {
        let val = repl.get_opt(opt);
        switch (typeof val) {
        case "boolean":
            var type = "checkbox";
            var event = `onchange="repl.update_opt('boolean', this.id, this.checked)"`;
            var def_val = val ? "checked" : "";
            break;
        case "number":
            var type = "number";
            var event = `onchange="repl.update_opt('number', this.id, this.value)"`;
            var def_val = `value="${val}"`;
            break;
        default:
            throw new Error(`Option type unrecognized: ${typeof repl.get_opt(opt)}`);
            break;
        };
        html = html + `<tr><td>"${opt}"</td><td id="${opt}_display" style="text-align:center">${repl.get_opt(opt)}</td><td style="text-align:center"><input id="${opt}" type="${type}" ${event} ${def_val}></td><td>${repl.options[opt].description}</td></tr>`;
    };
    html = html + "</table>";
    document.getElementById("settings").innerHTML = html;
};


////////////////////////////////////////////////////////////////////////////////
// emulation

// emulate a program with html logging on a new emulator
async function emulate_example_html(program) {
    if (program === undefined) { return ""; };
    let code = program.split('\n');
    html = `// CODE`;
    let align = Math.max(html.length, ...code.map((x) => { return x.length })) + 5;
    html = `<div style='font-weight:bold' class='nekocomment'>${html}${"&nbsp;".repeat(align - html.length)}// STACK</div>`
    let emu = new Emulator();
    emu.set_opt("max_while_iter", 20);
    emu.set_opt("max_call_depth", 20);
    for (let i=0; i<code.length; i++) {
        let parse = parse_program(code[i]);
        html = html + parse.html;
        let result = await emu.run(parse.commands, parse.definitions);
        html = `${html}<span class='nekocomment'>${"&nbsp;".repeat(align - code[i].length)}// ${nekos_to_string(emu.stack, true)}${result.error === undefined ? "" : "</span><br><span class='stderr'><b>ERROR</b>: " + result.error }</span><br>`;
    };
    html = `<div class='codeblock' style='margin-bottom:1em'>${html}</div>`
    return html;
};


////////////////////////////////////////////////////////////////////////////////
// pretty printers and reformatters

////////////////////////////////////////
// general helpers

// print a bunch of nekos in sequence
function nekos_to_string(nekos, reverse=false) {
    let strs = nekos.map(x => x.string());
    if (reverse) { strs.reverse(); };
    return strs.join(' ');
};

// safely convert a string to HTML
function make_html(text) {
    // TODO: this should probably be done more carefully...
    const elt = document.createElement("textarea");
    elt.innerText = text;
    return elt.innerHTML
        .replace(/\t/g, "  ")          // convert tabs to spaces
        .replace(/\n /g, "<br>&nbsp;") // manage initial line spaces
        .replace(/  /g, " &nbsp;")     // convert long spaces
        .replace(/\n/g, "<br>");       // convert other newlines
};

// make a nekoclassed html element
function stylize_html(cls, txt) {
    return `<span class="${"neko" + cls}">${make_html(txt)}</span>`;
};

////////////////////////////////////////
// documentation

// document `defn`s in order of recency of definition
function definitions_pretty_html(definitions) {
    let answer = "";
    for (let def in definitions) {
        answer = `<p><details><summary><span class="nekokeyword">defn</span> <span class="nekodefname">${def}</span></summary><div class="nekodefncontent codeblock"><span class="nekodelim">{</span><br><div class="nekodefncontent indented">${definitions[def].html}</div><span class="nekodelim">}</span></div></details></p>\n${answer}`;
    };
    return answer;
};

// make an html entry documenting a builtin
async function document_builtin(name) {
    let builtin = builtins[name];
    let example =
        builtin.example !== undefined ?
        await emulate_example_html(builtin.example) :
        "";
    let answer = `<p><details id="builtin_${name}_doc"><summary class="nekotypesignature"><code><span class="nekobuiltin">${name}</span> : <span class="nekotype">${builtin.type.input}</span> &rarr; <span class="nekotype">${builtin.type.output}${builtin.pure ? "" : " + STATE"}</span></code></summary><div class="nekodocs">${builtin.docstring.trim().split(/\n{2,}/).map(x => "<p>" + x + "</p>").join('\n')}${example}${(builtin.errors === []) ? "" : "<div class=\"errordescription\">Errors if...<ul>" + builtin.errors.map((i) => "<li>" + i + "</li>").join('') + "</ul></div>"}</div></details></p>`;
    return answer;
};

// create an html entry for every builtin at the time of call
async function document_all_builtins() {
    let answer = "";
    for (let key in builtins) {
        answer = answer + await document_builtin(key);
    };
    document.getElementById('builtins').innerHTML = answer;
};


////////////////////////////////////////////////////////////////////////////////
// fileviewer interface

function Fileviewer(libs, demos) {
    this.button = document.getElementById("demobutton");
    this.stderr = document.getElementById("demoerror");
    this.name = document.getElementById("demoname");
    this.code = document.getElementById("democode");
    this.imports = {};
    document.getElementById("nekofiles").innerHTML = `<option>None</option><optgroup label="Libraries">${libs.map((name) => "<option>lib/" + name + "</option>").join('')}</optgroup><optgroup label="Demos">${demos.map((name) => "<option>demo/" + name + "</option>").join('')}</optgroup>`;
};

// update the fileviewer interface
Fileviewer.prototype.update = async function () {
    let name = document.getElementById("nekofiles").value;
    let code_html, error_html;
    if (name == "None") {
        code_html = "";
        error_html = "";
    } else if (this.imports[name] !== undefined) {
        code_html = this.imports[name].html;
        error_html = "";
    } else {
        // we need to highlight syntax
        let code;
        if (this.imports[name] === undefined) {
            // we need to fetch the file and parse it
            // prevent further requests while fetching
            this.button.disabled = true;
            this.stderr.innerHTML = "Loading file, please wait...";
            let response = await fetch("" + name + ".nya");
            if (! response.ok) {
                this.stderr.innerHTML = "Error loading file, try again.";
                this.button.disabled = false;
                return;
            };
            this.stderr.innerHTML = "Parsing file...";
            let data = await response.text();
            let parse = parse_program(data);
            if (parse.error !== undefined) {
                this.stderr.innerHTML = `File did not parse correctly...<br>ERROR: ${parse.error} @ ${JSON.stringify(parse.start)} -- ${JSON.stringify(parse.end)}.<br>Please report this to a maintainer!`;
                // display the file without special formatting
                this.name.innerHTML = name;
                this.code.innerHTML = make_html(data);
                this.button.disabled = false;
                return;
            };
            this.imports[name] = parse;
        };
        code_html = this.imports[name].html;
    };
    this.name.innerHTML = name;
    this.code.innerHTML = code_html;
    this.stderr.innerHTML = "";
    this.button.disabled = false;
};


////////////////////////////////////////////////////////////////////////////////
// builtins

// collection of registered builtins
let builtins = {};

// for ease of writing documentation
const PURE = true;
const IMPURE = false;

// TODO: can the constructor also register the builtin?
function Builtin(name, action, type, pure, docstring, example, errors) {
    this.name = name;
    this.action = action;
    this.type = type;
    this.pure = pure;
    this.docstring = docstring;
    this.example = example;
    this.errors = errors;
};

// register new builtin
function builtin_register(name, func, type, pure, doc, ex, err) {
    builtins[name] = new Builtin(name, func, type, pure, doc, ex, err);
};

// register builtin for binary operation
function builtin_binary_register(name, ityp1, ityp2, otyp, op, doc, ex, err) {
    builtins[name] = new Builtin(
        name,
        (emulator) => {
            let check = emulator.check_stack(ityp1, ityp2);
            if (check !== undefined) { return check; };
            var x = emulator.pop().value;
            var y = emulator.pop().value;
            let val = new Neko(otyp, op(x, y), emulator, x, y);
            if (val.error !== undefined) { return val; };
            emulator.push(val);
        },
        {
            input: `${ityp1 ? ityp1 : "x"} ${ityp2 ? ityp2 : "y"} REST`,
            output: `${otyp} REST`,
        },
        PURE,
        doc,
        ex,
        err,
    );
};

// register builtin for unary operation
function builtin_unary_register(name, ityp, otyp, op, doc, ex, err) {
    builtins[name] = new Builtin(
        name,
        (emulator) => {
            let check = emulator.check_stack(ityp);
            if (check !== undefined) { return check; };
            var x = emulator.pop().value;
            let val = new Neko(otyp, op(x), emulator, x);
            if (val.error !== undefined) { return val; };
            emulator.push(val);
        },
        { input: `${ityp ? ityp : "x"} REST`, output: `${otyp} REST` },
        PURE,
        doc,
        ex,
        err,
    );
};

////////////////////////////////////////
// builtin code
//
// these should all be registered with one of the functions above.

builtin_register(
    "pop",
    (emulator) => {
        let check = emulator.check_length(1);
        if (check !== undefined) { return check; };
        emulator.pop();
    },
    { input: "x REST", output: "REST" },
    PURE,
    "Removes the top element of the stack.",
    "1 2 3\npop\npop\npop\npop",
    ["the stack is empty"],
);

builtin_register(
    "quote",
    (emulator) => {
        let check = emulator.check_length(1);
        if (check !== undefined) { return check; };
        var x = emulator.pop();
        emulator.push(new Neko("quote", [x], emulator, x.value));
    },
    { input: "x REST", output: "quote REST" },
    PURE,
    "Quotes the top element of the stack.",
    "1\nquote\nquote\npop\nquote",
    ["the stack is empty"],
);

builtin_register(
    "dup",
    (emulator) => {
        let check = emulator.check_length(1);
        if (check !== undefined) { return check; };
        var tmp = emulator.pop();
        emulator.push(tmp);
        emulator.push(tmp);
    },
    { input: "x REST", output: "x x REST" },
    PURE,
    "Duplicates the top element of the stack, placing the copy on top.",
    "0\ndup\npop pop\ndup",
    ["the stack is empty"],
);

builtin_register(
    "swap",
    (emulator) => {
        let check = emulator.check_length(2);
        if (check !== undefined) { return check; };
        var x = emulator.pop();
        var y = emulator.pop();
        emulator.push(x);
        emulator.push(y);
    },
    { input: "x y REST", output: "y x REST" },
    PURE,
    "Swaps the top two elements of the stack.",
    "1 2\nswap\nswap\npop\nswap\npop\nswap",
    ["the stack has height less than two"],
);

builtin_register(
    "apply",
    async (emulator) => {
        let check = emulator.check_stack("quote");
        if (check !== undefined) { return check; };
        var quote = emulator.pop();
        emulator.push_call(quote);
        let result = await emulator.apply(quote.value);
        if (result !== undefined) { return result };
        emulator.pop_call();
    },
    { input: "quote REST", output: "NEW" },
    PURE,
    "Applies a quote as a function to the rest of the stack.",
    "[1 2]\napply\npop pop\napply\n'string'\napply\npop [unrecognized name]\napply",
    [
        "the stack is empty",
        "the top element of the stack is not a quote",
        "the ACTION signals an error (propogated to the emulator)",
    ],
);

builtin_binary_register(
    "compose",
    "quote", "quote", "quote",
    (x, y) => { return x.concat(y) },
    `
Composes two quotes into one.\n
If \`A B REST\` &rarr; \`C REST\` as the result of compose, then the quote \`C\`
when applied is equivalent to running \`A apply B apply\`.
`,
    "[second] [first]\ncompose\ncompose\npop\ncompose\n[] 'string'\ncompose\nswap\ncompose",
    [
        "the stack has fewer than two elements",
        "the first element of the stack is not a quote",
        "the second element of the stack is not a quote",
    ],
);

builtin_register(
    "cond",
    (emulator) => {
        var check = emulator.check_length(3);
        if (check !== undefined) { return check; };
        var check = emulator.check_types("boolean", null, emulator.peek(2).type);
        if (check !== undefined) { return check; };
        if (emulator.pop().value) {
            emulator.stack[emulator.stack.length-2] = emulator.pop();
        } else {
            emulator.pop();
        }
    },
    { input: "boolean x x REST", output: "x REST" },
    PURE,
    `
Conditionally keeps exactly one of the second and third elements of the stack.
If the boolean on top is true, keeps the second element, and otherwise keeps the
third element.
`,
    "'no' 'yes' true\ncond\n'no' swap false\ncond\npop\ncond\n3\ncond\n[quote]\ncond\n'string'\ncond\npop true\ncond",
    [
        "the stack has fewer than three elements",
        "the top element on the stack is not a boolean",
        "the second and third elements do not have the same type",
    ],
);

builtin_register(
    "while",
    async (emulator) => {
        let check = emulator.check_stack("quote", "quote");
        if (check !== undefined) { return check; };
        let predNeko = emulator.pop();
        let funcNeko = emulator.pop();
        let pred = predNeko.value;
        let func = funcNeko.value;
        let i = 0;
        while (i <= emulator.get_opt("max_while_iter")) {
            i++;
            emulator.push_call(predNeko);
            var cond = await emulator.apply(pred);
            if (cond !== undefined) { return cond };
            let top = emulator.peek(1);
            if (top === undefined || top.type !== "boolean") {
                return new NekoError(
                    "`while` condition not boolean",
                    "the `while` condition failed to leave a boolean at the top of the stack",
                    emulator, emulator.peek(1),
                );
            };
            emulator.pop_call();
            if (emulator.pop().value === true) {
                emulator.push_call(funcNeko);
                let action = await emulator.apply(func);
                if (action !== undefined) { return action };
                emulator.pop_call();
            } else {
                break;
            };
        };
        if (i >= emulator.get_opt("max_while_iter")) {
            return new NekoError(
                `\`while\` terminated early (${emulator.get_opt("max_while_iter")} iterations)`,
                "change `max_while_iter` option via `setopt`",
                emulator,
            );
        };
    },
    { input: "quote quote REST" , output: "NEW" },
    PURE,
    `
Enact a while loop using a PREDICATE at the top of the stack and an ACTION in
the second position.\n
While the PREDICATE leaves a \`true\` at the top of the stack, the action is
evaluated.  Once it leaves a \`false\`, the loop is terminated.\n
The \`max_while_iter\` option controls how many iterations are allowed
before the emulator will force the loop to terminate.  This safeguard cannot be
disabled, but the stop value can be an arbitrarily big JavaScript integer.
`,
    "0 [-1 swap sub] [dup 10 swap lt]\nwhile\npop\nwhile\n[never evaluated]\nwhile\n1\nwhile\nswap\nwhile\nswap pop [errors out]\nwhile\n[errors out] [true]\nwhile\n[never evaluated] [1]\nwhile\npop [] [true]\nwhile",
    [
        "the stack has fewer than two elements",
        "the PREDICATE is not a quote",
        "the ACTION is not a quote",
        "the PREDICATE errors (propogated to the emulator)",
        "the ACTION errors (propogated to the emulator)",
        "the PREDICATE leaves a non-boolean at the top of the stack",
        "the evaluation takes the emulator more than `max_while_iter` steps",
    ],
);

builtin_binary_register(
    "sub",
    "integer", "integer", "integer",
    (x, y) => { return x - y },
    "Subtracts the second element of the stack from the first.",
    "-35 34\nsub\npop\nsub\n1\nsub\n'string'\nsub\nswap\nsub",
    [
        "the stack has fewer than two elements",
        "the first element of the stack is not a integer",
        "the second element of the stack is not a integer",
        "the result exceeds the JavaScript integer capacity", // TODO: example
    ],
);

// builtin_binary_register(
//     "add",
//     "integer", "integer", "integer",
//     (x, y) => { return x + y },
//     "Adds the top two elements of the stack.",
//     "230 190\nadd\npop\nadd\n1\nadd\n'string'\nadd\nswap\nadd",
//     [
//         "the stack has fewer than two elements",
//         "the first element of the stack is not an integer",
//         "the second element of the stack is not an integer",
//         "the result exceeds the JavaScript integer capacity", // TODO: example
//     ],
// );

// builtin_unary_register(
//     "neg",
//     "integer", "integer",
//     (x) => { return -x },
//     "Negates the top element of the stack.",
//     "42\nneg\npop\nneg\n'string'\nneg",
//     [
//         "the stack has fewer than two elements",
//         "the first element of the stack is not a integer",
//     ],
// );

builtin_binary_register(
    "mul",
    "integer", "integer", "integer",
    (x, y) => { return x * y },
    "Multiplies the top two elements of the stack.",
    "3 23\nmul\npop\nmul\n1\nmul\n'string'\nmul\nswap\nmul",
    [
        "the stack has fewer than two elements",
        "the first element of the stack is not a integer",
        "the second element of the stack is not a integer",
        "the result exceeds the JavaScript integer capacity", // TODO: example
    ],
);

builtin_register(
    "expt",
    (emulator) => {
        let check = emulator.check_stack("integer", "integer");
        if (check !== undefined) { return check; };
        if (emulator.peek(2).value < 0) {
            return new NekoError(
                "negative exponent",
                `see documentation for \`${emulator.last_call().string()}\``,
                emulator, emulator.peek(1), emulator.peek(2),
            );
        };
        var b = emulator.peek(1).value;
        var e = emulator.peek(2).value;
        let val = neko_number(b ** e, emulator, b, e);
        if (val.error !== undefined) { return val; };
        emulator.pop();
        emulator.pop();
        emulator.push(val);
    },
    { input: "integer integer REST", output: "integer REST" },
    PURE,
    "Exponentiates the first element of the stack by the second.",
    "5 2\nexpt\npop\nexpt\n1\nexpt\n'string'\nexpt\nswap\nexpt\nswap pop -1 swap\nexpt\npop pop 1000 10\nexpt",
    [
        "the stack has fewer than two elements",
        "the first element of the stack is not a integer",
        "the second element of the stack is not a integer",
        "the second element of the stack is negative",
        "the result exceeds the JavaScript integer capacity", // TODO: example
    ],
);

builtin_register(
    "qrt",
    (emulator) => {
        let check = emulator.check_stack("integer", "integer");
        if (check !== undefined) { return check; };
        if (emulator.peek(2).value == 0) {
            return new NekoError(
                "division by 0",
                `see documentation for \`${emulator.last_call().string()}\``,
                emulator, emulator.peek(1), emulator.peek(2),
            );
        };
        var n = emulator.pop().value;
        var d = emulator.pop().value;
        var abs_d = Math.abs(d);
        var sign_d = Math.sign(d);
        let r = neko_number((n % abs_d + abs_d) % abs_d, emulator, n, d);
        if (r.error !== undefined) { return r; };
        let q = neko_number(sign_d * Math.floor(n / abs_d), emulator, n, d);
        if (q.error !== undefined) { return q; };
        emulator.push(r);
        emulator.push(q);
    },
    { input: "integer integer REST", output: "integer integer REST" },
    PURE,
    `
Applies the Quotient-Remainder Theorem to the top two elements of the stack.\n
This is sometimes called the Division Algorithm, and the corresponding function
is called something like \`divmod\` in many languages.\n
The stack \`NUM DIV REST\` is transformed into \`QUOT REM REST\`.
`,
    "12 42\nqrt\npop pop 12 -42\nqrt\npop pop -12 42\nqrt\npop pop -12 -42\nqrt\npop pop\nqrt\n0\nqrt\n'string'\nqrt\nswap\nqrt\nswap pop 5\nqrt",
    [
        "the stack has fewer than two elements",
        "the first element of the stack is not a integer",
        "the second element of the stack is not a integer",
        "the second element of the stack is zero",
    ],
);

builtin_register(
    "rand",
    (emulator) => {
        // NOTE: look into randomness biases
        let n = Math.floor(Math.random() * Number.MAX_SAFE_INTEGER);
        let s = Math.sign(Math.random() * 2 - 1);
        emulator.push(neko_number(s * n, emulator));
    },
    { input: "REST", output: "integer REST" },
    IMPURE,
    "Pushes a random integer on top of the stack.",
    "rand\nrand\nrand",
    ["JavaScript nonsense"],
);

builtin_register(
    "slen",
    (emulator) => {
        emulator.push(neko_number(emulator.stack.length, emulator));
    },
    { input: "REST", output: "integer REST" },
    PURE,
    "Pushes the current length of the stack to the top of the stack.",
    "slen\npop 'elt 1' 'elt 2' 'third element' 'element four'\nslen",
    ["JavaScript nonsense"],
);

builtin_binary_register(
    "lt",
    "integer", "integer", "boolean",
    (x, y) => { return x < y },
    `
Checks if the first element of the stack is less than the second element of the
stack.
`,
    "42 69\nlt\npop 12 4\nlt\npop 1 1\nlt\npop\nlt\n1\nlt\n'string'\nlt\nswap\nlt",
    [
        "the stack has fewer than two elements",
        "the first element of the stack is not an integer",
        "the second element of the stack is not an integer",
    ],
);

// NOTE: this can't be `binary_register`ed because we need data on the types
builtin_register(
    "eq",
    (emulator) => {
        let check = emulator.check_length(2);
        if (check !== undefined) { return check; };
        check = neko_equality_check(emulator.peek(1), emulator.peek(2));
        if (check.error !== undefined) { return check; }
        emulator.push(new Neko(
            "boolean",  check,
            emulator, emulator.pop().value, emulator.pop().value));
    },
    { input: "x y REST", output: "boolean REST" },
    PURE,
    `
Compares the top two elements of the stack for equality.\n
Neko equality is equality as expressions.  In particular, if the two elements
look like they should be the same, then they are the same regardless of whatever
bullshit JavaScript says.  In particular, quotes are evaluated on deep equality,
booleans are only equal to booleans, integers to integers, etc.`,
    "'string' 'string'\neq\npop [quote] [quote]\neq\npop [quote] [[]]\neq\npop 1 7\neq\npop 'string' [quote]\neq\npop\neq\n1\neq",
    ["the stack has fewer than two elements"],
);

builtin_binary_register(
    "or",
    "boolean", "boolean", "boolean",
    (x, y) => { return x || y },
    "Takes the logical or of the top two elements of the stack.",
    "true true or\npop true false or\npop false true or\npop false false or\npop\nor\ntrue\nor\n1\nor\nswap\nor",
    [
        "the stack has fewer than two elements",
        "the first element of the stack is not a boolean",
        "the second element of the stack is not a boolean",
    ],
);

builtin_unary_register(
    "not",
    "boolean", "boolean",
    (x) => { return ! x },
    "Takes the logical not of the top element of the stack.",
    "true not\npop false not\npop\nnot\n'string'\nnot",
    [
        "the stack is empty",
        "the top element of the stack is not a boolean",
    ],
);

builtin_binary_register(
    "concat",
    "string", "string", "string",
    (x, y) => { return x + y },
    "Concatenates two strings at the top of the stack.",
    "'second' ' and ' 'first'\nconcat\nconcat\npop\nconcat\n'string'\nconcat\n1\nconcat\nswap\nconcat",
    [
        "the stack has fewer than two elements",
        "the top element of the stack is not a string",
        "the second element of the stack is not a string",
    ],
);

builtin_register(
    "split",
    (emulator) => {
        let check = emulator.check_stack("integer", "string");
        if (check !== undefined) { return check; };
        let n = emulator.peek(1).value;
        let str = emulator.peek(2).value;
        if (n < 0 || str.length < n) {
            return new NekoError(
                "out of bounds",
                `see documentation for \`${emulator.last_call().string()}\``,
                emulator, n, str,
            );
        };
        emulator.pop();
        emulator.pop();
        emulator.push(new Neko("string", str.slice(n), emulator, n, str));
        emulator.push(new Neko("string", str.slice(0, n), emulator, n, str));
    },
    { input: "integer string REST", output: "string string REST" },
    PURE,
    `
Splits a string at a given index.\n
Strings are zero-indexed.  This function effectively transforms the stack like
so: \`n string REST\` &rarr; \`string[0:n] string[n:] REST\`.
`,
    "'i am a string' 3\nsplit\npop pop 'string' 6\nsplit\n0\nsplit\npop pop pop pop\nsplit\n'string'\nsplit\n'not an integer'\nsplit\npop pop [not a string] 1\nsplit\npop pop 'string' -1\nsplit\npop 10\nsplit",
    [
        "the stack has fewer than two elements",
        "the top element of the stack is not an integer",
        "the second element of the stack is not a string",
        "the top element of the stack is negative",
        "the top element of the stack is larger than the length of the string",
    ],
);

builtin_register(
    "str2int",
    (emulator) => {
        let check = emulator.check_stack("string");
        if (check !== undefined) { return check; };
        // TODO: this is incorrect, because parseInt is stupid
        //       e.g. parseInt("42c") == 42
        //       need to write an integer parser from scratch
        let str = emulator.peek(1).value;
        let num = parseInt(str);
        let is_num = ! isNaN(num);
        let res = neko_number(is_num ? num : 0, emulator, str);
        if (res.error !== undefined) { return res; };
        emulator.pop();
        emulator.push(res);
        emulator.push(new Neko("boolean", is_num, emulator, str));
    },
    { input: "string REST", output: "boolean integer REST" },
    PURE,
    `
Converts a string representing an integer in base 10 into that integer.\n
The function pushes two elements onto the stack: BOOL and INT.  BOOL tracks
whether or not the function succeeded (i.e., if the string represented an
integer in base 10).  INT is either that integer (in the success case) or 0 (in
the failure case).\n
Note that this function is currently held back by the limitations and general
bullshit that is JavaScript.  Integers that overflow JavaScript cause an error,
and some strings that should not parse to integers do.  There are plans to
replace the current integer model by one that would be (nearly) unbounded and
protect against other pathologies of JavaScript.
`,
    "'1234567890'\nstr2int\npop pop 'a43'\nstr2int\npop pop '43a'\nstr2int\npop pop [not a string]\nstr2int",
    [
        "the stack is empty",
        "the top element of the stack is not a string",
        "the top element of the stack represents an integer overflowing JavaScript",
    ],
);

builtin_register(
    "show",
    (emulator) => {
        let check = emulator.check_length(1);
        if (check !== undefined) { return check; };
        var x = emulator.pop();
        let str =
            x.type == "string" ?
            x :
            new Neko("string", x.string(), emulator, x.value);
        emulator.push(str);
    },
    { input: "x REST", output: "string REST" },
    PURE,
    "Turns the top element of the stack into a string.",
    "1\nshow\npop 'string'\nshow\npop [quote and such]\nshow\npop\nshow",
    ["the stack is empty"],
);

builtin_register(
    "forget",
    (emulator) => {
        let check = emulator.check_stack("string");
        if (check !== undefined) { return check; };
        var name = emulator.peek(1).value;
        if (emulator.definitions[name] === undefined) {
            return new NekoError(
                "undefined name",
                `see documentation for \`${emulator.last_call().string()}\``,
                emulator, name,
            );
        };
        emulator.pop();
        delete emulator.definitions[name];
        emulator.defn_update = true;
    },
    { input: "string REST", output: "REST" },
    IMPURE,
    `
Removes the definition whose name is at the top of the stack from the list of
definitions.
`,
    "defn test { 1 2 3 }\ntest\npop pop pop 'test'\nforget\ntest",
    [
        "the stack is empty",
        "the first element of the stack is not a string",
        "the string on top of the stack is not the name of a stored definition",
    ],
);

// TODO: pre-process `load`s to determine code dependence tree
builtin_register(
    "load",
    async (emulator) => {
        let check = emulator.check_stack("string");
        if (check !== undefined) { return check; };
        let name = emulator.peek(1).value;
        if (emulator.imports[name] === undefined) {
            let response = await fetch("" + name + ".nya")
                .catch(err => new NekoError(
                    "`load` encountered a network error",
                    "contact a maintainer and explain what you were doing when this happened",
                    emulator, name
                ));
            if (! response.ok) {
                return new NekoError(
                    "`load` failed",
                    "requested file may not exist (check spelling and/or contact a maintainer)",
                    emulator, name,
                );
            } else if (response.error !== undefined) { return response; };
            let program = await response.text();
            let parse = parse_program(program);
            if (parse.error !== undefined) { return parse; };
            emulator.imports[name] = parse;
            emulator.defn_update = Object.keys(parse.definitions).length > 0;
        };
        emulator.pop();
        let data = emulator.imports[name];
        let result = await emulator.run(data.commands, data.definitions);
        if (result.error !== undefined) { return result; };
    },
    { input: "string REST", output: "NEW" },
    IMPURE,
    `
Loads a Neko file and executes it as if its contents appeared at this position
in the code.\n
The string on top of the stack should reference a file by its path (excluding
the \`.nya\` file extension).
`,
    undefined,
    [
        "the stack is empty",
        "the top element of the stack is not a string",
        "the loaded file causes an error (propogated to the emulator)",
        "other cases, probably...",
    ],
);

////////////////////////////////////////
// repl commands

builtin_register(
    "stdin",
    async (repl) => {
        await new Promise(
            (resolve) => {
                repl.html.activeprompt.innerHTML = "??&gt; ";
                // TODO: need a listener to reset the REPL during a call...
                repl.html.stdin.removeEventListener("keydown", repl_listener);
                repl.html.stdin.addEventListener("keydown", repl_stdin_handler);
                // enable stdin
                repl.html.stdin.disabled = false;
                repl.html.stdin.value = "";
                repl.html.stdin.focus();
                repl.html.stdin.scrollIntoView(false);
                window.scrollBy(0, 50);
                repl.sync_stack();
                function repl_stdin_handler(e) {
                    switch (e.key) {
                    case "Enter":
                        let input = repl.html.stdin.value;
                        // TODO: fix the escaping
                        repl.push(new Neko(
                            "string",
                            input
                                .replace(/\\n/g, "\n")
                                .replace(/\\t/g, "\t")
                                .replace(/\\/g, "\\\\"),
                            repl));
                        repl.html.activeprompt.innerHTML = prompt;
                        repl.sync_stack();
                        repl.html.stdin.removeEventListener("keydown", repl_stdin_handler);
                        repl.html.stdin.addEventListener("keydown", repl_listener);
                        repl.html.stdin.disabled = true;
                        resolve();
                    default:
                    };
                };
            },
        );
    },
    { input: "REST", output: "string REST" },
    IMPURE,
    `
Wait for a return signal from standard input, and then store the contents as a
string on the stack.
`,
    undefined,
    ["some stupid JavaScript thing happens, probably"],
);

builtin_register(
    "stdout",
    (repl) => {
        let check = repl.check_stack("string");
        if (check !== undefined) { return check; };
        var string = repl.pop().value;
        repl.stdout(make_html(string).replace(/\\n/g, "<br>"));
    },
    { input: "string REST", output: "REST" },
    IMPURE,
    "Print the string on the top of the stack to the standard output.",
    undefined,
    [
        "the stack is empty",
        "the first element of the stack is not a string",
        "some stupid JavaScript thing happens, probably",
    ],
);

builtin_register(
    "setopt",
    (repl) => {
        let check = repl.check_stack("string", null);
        if (check !== undefined) { return check; };
        var opt_name = repl.peek(1).value;
        let old_value = repl.get_opt(opt_name);
        if (old_value === undefined) {
            return new NekoError(
                "undefined option",
                "an option by this name is not available",
                repl, opt_name,
            );
        };
        var new_value = repl.peek(2).value;
        if ((typeof old_value) !== (typeof new_value)) {
            return new NekoError(
                "incompatable type",
                "it is not possible to set an option's value to a new type",
                repl, opt_name, new_value,
            );
        };
        // consume the arguments
        repl.pop();           // option name
        repl.pop();           // new value
        // set the option
        repl.update_opt(typeof old_value, opt_name, new_value);
    },
    { input: "string value REST", output: "REST" },
    IMPURE,
    `
Sets an option in the REPL environment.\n
There are a variety of options to control how the REPL environment runs.  This
function sets these options from within the REPL.
`,
    undefined,
    // "10 'max_while_iter'\nsetopt\nsetopt\n4\nsetopt\n'not_an_option'\nsetopt\npop pop true 'max_while_iter'\nsetopt",
    [
        "the length of the stack is less than two",
        "the first element of the stack is not a string",
        "the first element of the stack does not reference an option",
        "the second element of the stack has the wrong type for the option",
    ],
)

builtin_register(
    "clear",
    (repl) => {
        // clear the display
        repl.html.stdout.innerHTML = "";
        // forget inaccessable history items
        repl.history = [];
    },
    { input: "REST", output: "REST" },
    IMPURE,
    "Clears the stdout.",
    undefined,
    [],
);

builtin_register(
    "reset",
    (repl) => {
        // save previous fetches
        imports = repl.imports;
        // destroy current state
        delete repl;
        // refresh the state
        repl_initialize();
        // restore state data
        repl.imports = imports;
    },
    { input: "REST", output: "&empty;" },
    IMPURE,
    `
Resets the REPL.\n
This is a hard reset.  It renews most aspects of the REPL, effectively starting
over.  This includes definitions, the stack, and all options set during the
session.  The only thing preserved is the list of imports.
`,
    undefined,
    [],
);
