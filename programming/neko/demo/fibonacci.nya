"lib/stdlib" load

// construct the nth fibonacci number
defn fibonacci {                // n
  // setup
  1 swap 0 swap                 // n 0 1
  // action
  [                             // n fk fk-1
    1 swap sub                  // n-1 fk fk-1
    perm231                     // fk fk-1 n-1
    dup                         // fk fk fk-1 n-1
    perm231                     // fk fk-1 fk n-1
    add                         // fk+1 fk n-1
    perm312                     // n-1 fk+1 fk
  ]
  // predicate
  [                             // n
    dup 0 lt                    // 0<n n
  ]
  while                         // 0 fn fn-1
  // cleanup
  pop swap pop                  // fn
}

// construct a list of n+1 fibonacci numbers
defn fibonacci_list {           // n rest
  // setup
  0 swap 1 swap                 // n 1 0 rest
  dup 0 lt                      // n>0 n 1 0 rest
  // if n <= 0, get rid of the 1
  [pop] swap
  // if n > 0, enact the following while
  [// action
    [                             // n fn fn-1 rest
      1 swap sub                  // n-1 fn fn-1 rest
      quote                       // [n-1] fn fn-1 rest
      [quote swap quote compose]  // [quote swap quote compose] [n-1] fn fn-1 rest
      compose apply               // n-1 [fn-1 fn] rest
      quote swap dup              // [fn-1 fn] [fn-1 fn] [n-1] rest
      [add] swap                  // [fn-1 fn] [add] [fn-1 fn] [n-1] rest
      compose swap                // [fn-1 fn] [fn-1 fn add] [n-1] rest
      compose compose             // [fn-1 fn fn-1 fn add n-1] rest
      apply                       // n-1 fn+1 fn fn-1 rest
    ]
    // predicate
    [                             // n rest
      dup 1 lt                    // 0<n n rest
    ]
    while                         // 0 fn fn-1 ... f2 f1 f0 rest
  ] swap
  cond apply                    // apply appropriate actions
  // cleanup
  pop                           // fn fn-1 ... f2 f1 f0 rest
}
