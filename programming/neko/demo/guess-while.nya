// Looping implementation of a guessing game.
// Prompts the user to guess a number between 1 and 100.

// we will keep the stack in the following order: GUESS ANSWER TURNS

defn inc {
  // increment the top of the stack
                                // n _
  -1                            // -1 n _
  swap                          // n -1 _
  sub                           // (n+1) _
}

defn congratulate {
  // final message to congratulate the user on win
                                // a a t _
  pop                           // a t _
  ".  \n" swap show             // astr . t _
  "The answer was "             // was astr . t _
  "That's corect!  "            // right was astr . t _
  concat concat concat          // yes t _
  swap " turns." swap show      // tstr turns yes _
  "You guessed right in "       // num tstr turns yes _
  concat concat swap concat     // message _
  stdout                        // _
}

defn prompt {
  // check if answer is less than guess, and hint the user
                                // g a t _
  quote swap quote compose      // [a g] t _
  dup compose apply             // g a g a t' _
  pop lt                        // (a < g) a t' _
  ", guess again." swap         // (a < g) msg_tail a t' _
  "small" swap                  // (a < g) small msg_tail a t' _
  "big" swap                    // (a < g) big small msg_tail a t' _
  cond                          // size msg_tail a t' _
  "Too " concat concat          // msg a t' _
   stdout                       // a t' _
}

defn get_guess {
  // asks user to guess a number, looping until receiving an integer
                                // a t _
  swap inc swap                 // a (t+1) _
  [                          // n _
    pop                      // _
    "  Guess again..."       // cmd _
    "That's not an integer." // info cmd _
    concat stdout            // _
  ]
  [stdin str2int not]        // _ => b n _
  while                         // g a (t+1) _
  dup show                      // gstr g a (t+1) _
  "guess> " concat stdout       // g a (t+1) _
}

defn check_guess {
  // check if answer is correct and increment the turn counter
                                // g a t _
  quote swap quote compose      // [a g] t _
  dup compose apply             // g a g a t _
  eq not                        // (g =/= a) g a t _
}

defn play_game {
  // main game loop

  // turn stack printing off (to prevent answer leaks)
  false "show_stack" setopt
  // set up the stack with the answer and number of turns taken
                                // _
  0 100 rand qrt pop inc        // a 0 _
  // message the user to explain the game
  "Guess the number between 1 and 100." stdout
  // get the initial guess
  get_guess                     // g a 0 _
  // set up game loop
  [prompt get_guess]            // act g a 0 _
  [check_guess]                 // cond act g a 0 _
  // enact game loop
  while                         // a a t _
  // congratulate user
  congratulate                  // _
  // turn stack printing on
  true "show_stack" setopt
}

play_game
