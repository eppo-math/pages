"lib/stdlib" load

defn divides { swap qrt pop 0 eq }

// leap year calculation via quote construction
// constructs the logic to apply, then applies it
defn leap_via_logic {
  // y rest
  [or]
  // [or] y rest
  swap
  // y [or] rest
  quote
  // [y] [or] rest
  [divides] swap compose
  // [y divides] [or] rest
  dup [not and 400] swap
  // [y divides] [not and 400] [y divides] [or] rest
  dup [100] swap
  // [y divides] [100] [y divides] [not and 400] [y divides] [or] rest
  [4]
  // [4] [y divides] [100] [y divides] [not and 400] [y divides] [or] rest
  compose compose compose compose compose compose
  // [4 y divides 100 y divides not and 400 y divides or] rest
  apply
}

// leap year calculation via conditional application
// effectively short circuits the calculation if 400 divides year
defn leap_conditional {     // y rest
  dup                           // y y rest
  // test for false case
  [                             // x rest
    dup                         // x x rest
    100 divides not             // (not 100|x) x rest
    swap                        // x (not 100|) rest
    4 divides                   // (4|x) (not 100|x) rest
    and                         // (4|x and not 100|x) rest
  ]                             // [test-false-case] y y rest
  swap                          // y [test-false-case] y rest
  [pop true]                    // [pop true] y [test-false-case] y rest
  swap                          // y [pop true] [test-false-case] y rest
  400 divides                   // (400|y) [pop true] [test-false-case] y rest
  cond apply                    // (y-is-leap-year) rest
}

// leap year calculation via conditional application
// this does as much short circuiting as possible
defn leap_short {               // y rest
  dup                           // y y rest
  [pop false] swap              // y [false] y rest
  [                             // x rest
    dup                         // x x rest
    [pop true] swap             // x [pop true] x rest
    [400 divides] swap          // x [400-test] [pop true] x rest
    100 divides                 // (100|x) [400-test] [pop true] x rest
    cond apply                  // ((not 100|x) or (400|x)) rest
  ] swap                        // y [100-test] [pop false] y rest
  4 divides                     // (4|y) [100-test] [pop false] y rest
  cond apply                    // (y-is-leap-year) rest
}

// shortcut to make testing easier from reloads
defn leap { leap_conditional }
