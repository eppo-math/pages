"lib/stdlib" load

defn summarize_test {
  // result-as-bool name
  swap quote swap
  ["fail"] swap
  ["pass"] swap
  cond
  compose
}

defn test_sum {
  "sum"                         // test name
   15                           // expected result
   5 count 5 sum                // test action
   eq                           // test for equality
   summarize_test
}

// run some tests

"
This demo runs a few quick sanity checks to make sure most basic things are not
broken with the REPL.  You should type 'yes' at the prompt now to continue, or
anything else to abort the tests.  PLEASE READ THE NOTES BELOW.

NOTES:
 (1) The stack will be drained if you choose to continue.  If you need to save
     it for some reason, you should abort now.
 (2) The file will still run one command to forget a useless test definition.
     It is thus still possible for the file to error after this point...
 (3) If the `stdout` primitive fails, this will break immediately, and this
     message will be left at the top of the stack.

Is it OK to continue?"
stdout

// abort
[
  "Testing aborted." stdout
]

// continue
[
  "Testing starting..." stdout
  "Testing a number push..." stdout
  1
  "Testing `dup`..." stdout
  dup
  "Testing `pop`..." stdout
  pop
  "Testing `swap`..." stdout
  2 swap swap pop
  "Testing a quote push..." stdout
  [pop 0] [dup 0 eq not]
  "Testing `while`..." stdout
  while
  "Testing `apply`..." stdout
  [pop 1] apply
  // HERE
  "Testing `quote`..." stdout
  quote [2]
  "Testing `compose`..." stdout
  compose apply
  "Testing `add`..." stdout
  add
  "Testing `sub`..." stdout
  1 sub
  "Testing `mul`..." stdout
  5 mul
  "Testing `qrt`..." stdout
  42 qrt
  0 sub add 0 sub
  "Testing `expt`..." stdout
  2 expt
  "Testing `eq`..." stdout
  8 eq
  "Testing `or`..." stdout
  false or
  "Testing `not`..." stdout
  not pop
  "Testing `lt`..." stdout
  0 42 lt
  "Testing `rand`..." stdout
  rand pop
  "Testing `slen`..." stdout
  slen pop
  "Testing a call to named function `sdrain`..." stdout
  sdrain
  "Testing `setopt`..." stdout
  10 "max_while_iterations" setopt
  10000 "max_while_iterations" setopt
  "Testing `stdin`...
Please enter something at the prompt." stdout
  stdin
  "Testing `concat`..." stdout
  "You entered this string: " concat stdout
  // reset
  // "reset passed" stdout
  // "exiting now" stdout
  // exit
  "Tests have not caused any obvious errors." stdout
]

// check user response
stdin "yes" eq
// run the promised `forget`
defn test_sdgwthwvaeh5dfvaeb6uw44t { beep boop }
"Forgetting a defn called `test_sdgwthwvaeh5dfvaeb6uw44t`" stdout
"test_sdgwthwvaeh5dfvaeb6uw44t" forget
// run resulting operations
cond apply
