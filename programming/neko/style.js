// TODO: this file should generate all CSS styles
// less efficient than having a .css, but it eliminates two requests
// it also allows me to control theming from js (including a theme cookie)

let menu = [
    { text: "Home",  link: "./" },
    { text: "REPL",  link: "./repl.html" },
    { text: "Files", link: "./files.html",
      dropdown: [
          { text: "File Viewer",  link: "./files.html" },
          { text: "Standard Library",  link: "./stdlib.html" },
          { text: "Guessing Game",  link: "./guess.html" },
      ]
    },
    { text: "Docs",  link: "./docs.html",
      dropdown: [
          { text: "Documentation",  link: "./docs.html" },
          { text: "Standard Library",  link: "./stdlib.html" },
          { text: "File Viewer",  link: "./files.html" },
      ]
    },
    { text: "Math",  link: "./maths.html" },
];

let logo = `<a href="http://eppo-math.codeberg.page/">Neko@EppoMath</a>`

function dropdown_navbar_from_nested_list(logo, list) {
    // list has elements of this form:
    // { text, link, dropdown }
    let answer = `<div class="header"><div class="navbar"><div class="logo">${logo}</div><input type="checkbox" id="navbar-toggle" ><label for="navbar-toggle"><i></i></label><nav class="menu"><ul>`;
    for (let i=0; i<list.length; i++) {
        answer = answer +
            `<li class="dropdown"><button class="dropbtn">` +
            format_maybe_link(list[i]) + "</button>" +
            (list[i].dropdown !== undefined ? `<div class="dropdown-content">${list[i].dropdown.map(format_maybe_link).join("")}</div>` : "") +
            "</li>";
    };
    answer = answer + "</ul></nav></div></div>"
    return answer;
};

function format_maybe_link(entry) {
    return `<a${entry.link === undefined ? "" : " href=\"" + entry.link + "\""}>${entry.text}</a>`;
};

function navbar_create() {
    let navbar = dropdown_navbar_from_nested_list(logo, menu);
    document.getElementById('navbar').innerHTML = navbar;
};

function toggle_section(elt) {
    let indicator = elt.parentElement.children[0].children[0];
    let content = elt.parentElement.children[1];
    if (content.style.display == "none") {
        content.style.display = "block";
        indicator.innerHTML = "&#9660;";
    } else {
        content.style.display = "none";
        indicator.innerHTML = "&#9654;";
    };
};

function initialize_folding_sections() {
    let folding = document.getElementsByClassName("folding");
    for (let i=0; i<folding.length; i++) {
        let kids = folding[i].children;
        kids[0].outerHTML = `<div class="sechead" id="${folding[i].id}_head" style="display:flex;font-size:20px;" onclick="toggle_section(this);"><span style="width:2em">&#9654;</span><a style="width:100%" href="#${folding[i].id}_head">${kids[0].innerHTML}</a><span style="width:2em"></span></div>`;
        kids[1].style.display = "none";
        kids[1].style.margin = "3em";
    };
};

function replace_with_code(path) {
    // replaces element with id PATH by syntax highlighted code from that PATH
    fetch(path)
        .then(fil => fil.text())
        .then(code => parse_program(code).html)
        .then(html => document.getElementById(path).outerHTML = `<div class="codeblock">${html}</div>`);
};
